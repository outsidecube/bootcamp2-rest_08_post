const { query } = require('../repositories/main.repository');

class itemDao {
  static create(item) {
    const insertItemQuery = `INSERT INTO Item (invoiceId, description, price, units, discount) 
        values (?, ?, ?, ?, ?)`;
    return query(insertItemQuery, item);
  }

  static exists(value, field) {
    const sql = `SELECT COUNT(*) AS 'exists' FROM Item WHERE ${field} = ?`;
    return query(sql, value);
  }

  static get(id) {
    const sql = `SELECT id, invoiceId, description, price, units, discount, createdAt FROM Item WHERE id = ?`;
    return query(sql, id);
  }

  static getAll(filters) {
    let sql = `SELECT id, invoiceId, description, price, units, discount, createdAt FROM Item`;

    let where = '';
    const filterValues = [];

    for (let filter in filters) {
      if (filter === 'price') {
        where += `CAST(${filter} AS DECIMAL) = ? AND `
      } else {
        where += `${filter} = ? AND `
      }
      if (filter !== 'description') {
        filterValues.push(parseFloat(filters[filter]))
      } else {
        filterValues.push(filters[filter])
      }
    }

    if (where !== '') {
      where = where.substring(0, where.length - 4);
      sql += ` WHERE ${where}`
      console.log('QUERY: ', sql, filterValues);
      return query(sql, filterValues);
    } else {
      return query(sql);
    }
  }

  static delete(id) {
    const sql = `DELETE FROM Item WHERE id = ?`;
    return query(sql, id);
  }

  static deleteAllInvoiceItems(invoiceId) {
    const sql = `DELETE FROM Item WHERE invoiceId = ?`;
    return query(sql, invoiceId);
  }

  static update(id, description, price, units, discount) {
    let filters = '';
    const queryParams = [];
    let fields = 0;

    if (description) {
      filters += `description = ?`;
      queryParams.push(description);

      fields++;
    }

    if (price) {
      if (fields > 0) filters += `,`;

      filters += `price = ?`;
      queryParams.push(price);

      fields++;
    }

    if (units) {
      if (fields > 0) filters += `,`;

      filters += `units = ?`;
      queryParams.push(units);

      fields++;
    }

    if (discount) {
      if (fields > 0) filters += `,`;

      filters += `discount = ?`;
      queryParams.push(discount);

      fields++;
    }

    let sql = `UPDATE Item SET ${filters} WHERE id = ?`;

    queryParams.push(id);

    return query(sql, queryParams);
  }
}

module.exports = itemDao;
