const { query } = require('../repositories/main.repository');

class invoiceDao {
  static create(invoice) {
    const insertInvoiceQuery = `INSERT INTO Invoice (date, cuit, comment) 
        values (?, ?, ?)`;
    return query(insertInvoiceQuery, invoice);
  }

  static exists(value, field) {
    const sql = `SELECT COUNT(*) AS 'exists' FROM Invoice WHERE ${field} = ?`;
    return query(sql, value);
  }

  static get(id) {
    const sql = `SELECT id, date, cuit, comment, createdAt FROM Invoice WHERE id = ?`;
    return query(sql, id);
  }

  static getAll(filters) {
    let sql = `SELECT id, date, cuit, comment, createdAt FROM Invoice`;
    let where = '';
    const filterValues = [];

    for (let filter in filters) {
      where += `${filter} = ? AND `
      filterValues.push(filters[filter]);
    }

    if (where !== '') {
      where = where.substring(0, where.length - 4);
      sql += ` WHERE ${where}`
      return query(sql, filterValues);
    } else {
      return query(sql);
    }
  }

  static delete(id) {
    const sql = `DELETE FROM Invoice WHERE id = ?`;
    return query(sql, id);
  }

  static update(id, date, cuit, comment) {
    let filters = '';
    const queryParams = [];
    let fields = 0;

    if (date) {
      filters += `date = ?`;
      queryParams.push(date);

      fields++;
    }

    if (cuit) {
      if (fields > 0) filters += `,`;

      filters += `cuit = ?`;
      queryParams.push(cuit);

      fields++;
    }
    if (comment) {
      if (fields > 0) filters += `,`;

      filters += `comment = ?`;
      queryParams.push(comment);

      fields++;
    }

    let sql = `UPDATE Invoice SET ${filters} WHERE id = ?`;

    queryParams.push(id);

    return query(sql, queryParams);
  }
}

module.exports = invoiceDao;
