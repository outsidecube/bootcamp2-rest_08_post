const itemController = require('./item.controller');
const itemService = require('../services/item.service.js');

jest.mock('../services/item.service.js');

let sendMock;
let statusMock;
let res;

beforeEach(() => {
  sendMock = jest.fn();
  statusMock = jest.fn();
  res = { status: statusMock, send: sendMock };
  statusMock.mockImplementation(() => res);
});

describe('Item Controller', () => {
  describe('create method', () => {
    it('should fail with 400 if invoiceId is missing', async () => {
      const req = {
        body: {}
      };

      await itemController.create(req, res);
      expect(statusMock).toBeCalledWith(400);
    });

    it('should fail with 400 if price is missing', async () => {
      const req = {
        body: {
          invoiceId: 1
        }
      };

      await itemController.create(req, res);
      expect(statusMock).toBeCalledWith(400);
    });

    it('should fail with 400 if unit is missing', async () => {
      const req = {
        body: {
          invoiceId: 1,
          price: 10
        }
      };

      await itemController.create(req, res);
      expect(statusMock).toBeCalledWith(400);
    });

    it('should pass with 201 and create item', async () => {
      const req = {
        body: {
          invoiceId: 1,
          price: 10,
          units: 1,
          discount: 0,
          description: 'Test'
        }
      };
      const mock = {};
      itemService.create.mockImplementationOnce(() => mock);

      await itemController.create(req, res);
      expect(statusMock).toBeCalledWith(201);
    });

    it('should return with 500', async () => {
      const req = {
        body: {
          invoiceId: 1,
          price: 10,
          units: 1,
          discount: 3,
          description: 'Test'
        }
      };
      const mock = {};
      itemService.create.mockImplementationOnce(() => {
        throw new Error('Error');
      });

      await itemController.create(req, res);
      expect(statusMock).toBeCalledWith(500);
    });
  });

  describe('/put update item', () => {
    it('should pass with 200 and update item', async () => {
      const req = {
        params: {
          id: 1
        },
        body: {
          description: 'Test (1)'
        }
      };
      const mock = {};
      itemService.update.mockImplementationOnce(() => mock);

      await itemController.update(req, res);
      expect(statusMock).toBeCalledWith(200);
    });

    it('should return 400 bad request', async () => {
      const req = { params: { id: 1 }, body: { description: 1 } };

      await itemController.update(req, res);
      expect(statusMock).toBeCalledWith(400);
    });

    it('should return 404 item_not_found', async () => {
      const req = { params: { id: 1 }, body: {} };

      itemService.update.mockImplementationOnce(() => {
        throw {
          status: 404,
          error: 'item_not_found',
          msg: 'Item no encontrado'
        };
      });

      await itemController.update(req, res);
      expect(statusMock).toBeCalledWith(404);
    });

    it('should return with 500', async () => {
      const req = {
        params: {
          id: 1
        },
        body: {
          comment: 'Test (1)'
        }
      };
      const mock = {};
      itemService.update.mockImplementationOnce(() => {
        throw new Error('Error');
      });

      await itemController.update(req, res);
      expect(statusMock).toBeCalledWith(500);
    });
  });

  describe('delete an item', () => {
    it('should succeed with 200 and delete item', async () => {
      const req = { params: { id: 1 } };

      const mock = {};
      itemService.delete.mockImplementationOnce(() => mock);

      await itemController.delete(req, res);
      expect(statusMock).toBeCalledWith(204);
    });

    it('should return 404 item_not_found', async () => {
      const req = { params: { id: 1 } };

      const mock = {};
      itemService.delete.mockImplementationOnce(() => {
        throw {
          status: 404,
        error: 'item_not_found',
        msg: 'Item no encontrado'
        };
      });

      await itemController.delete(req, res);
      expect(statusMock).toBeCalledWith(404);
    });

    it('should return 500', async () => {
      const req = { params: { id: 1 } };

      const mock = {};

      itemService.delete.mockImplementationOnce(() => {
        throw new Error('Error');
      });

      await itemController.delete(req, res);
      expect(statusMock).toBeCalledWith(500);
    });
  });

  describe('get single item', () => {
    it('should pass with 200 ', async () => {
      const req = {
        params: { id: 1 }
      };
      const mock = [
        {
          id: 1,
          invoiceId: 1,
          description: 'Test Description',
          price: 1200,
          units: 1,
          discount: 0,
          createdAt: '2020-04-10T21:01:05.000Z'
        }
      ];

      const result = [
        {
          id: 1,
          invoiceId: 1,
          description: 'Test Description',
          price: 1200,
          units: 1,
          discount: 0,
          createdAt: '2020-04-10T21:01:05.000Z'
        }
      ];

      itemService.get.mockImplementationOnce(() => mock);

      await itemController.get(req, res);
      expect(statusMock).toBeCalledWith(200);
      expect(sendMock).toBeCalledWith(expect.objectContaining(result));
    });

    it('should return 404 - item_not_found', async () => {
      const req = {
        params: { id: 10 }
      };

      itemService.get.mockImplementationOnce(() => {
        throw {
          status: 404,
          error: 'item_not_found',
          msg: 'Item no encontrado'
        }
      });

      await itemController.get(req, res);
      expect(statusMock).toBeCalledWith(404);
    });
  });

  describe('get all invoices', () => {
    it('should pass with 200 ', async () => {
      const req = {
        params: {}
      };
      const mock = [
        {
          id: 1,
          invoiceId: 1,
          description: 'Test Description',
          price: 1200,
          units: 1,
          discount: 0,
          createdAt: '2020-04-10T21:01:05.000Z'
        },
        {
          id: 3,
          invoiceId: 1,
          description: 'Prueba',
          price: 35,
          units: 5,
          discount: 10,
          createdAt: '2020-04-10T21:02:30.000Z'
        }
      ];

      const result = [
        {
          id: 1,
          invoiceId: 1,
          description: 'Test Description',
          price: 1200,
          units: 1,
          discount: 0,
          createdAt: '2020-04-10T21:01:05.000Z'
        },
        {
          id: 3,
          invoiceId: 1,
          description: 'Prueba',
          price: 35,
          units: 5,
          discount: 10,
          createdAt: '2020-04-10T21:02:30.000Z'
        }
      ];

      itemService.getAll.mockImplementationOnce(() => mock);

      await itemController.get(req, res);
      expect(statusMock).toBeCalledWith(200);
      expect(sendMock).toBeCalledWith(expect.objectContaining(result));
    });

    it('should return 500 ', async () => {
      const req = {
        params: {}
      };

      itemService.getAll.mockImplementationOnce(() => {
        throw new Error('Error');
      });

      await itemController.get(req, res);
      expect(statusMock).toBeCalledWith(500);
    });
  });
});
