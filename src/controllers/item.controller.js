const itemService = require('../services/item.service');
const { generateUuid } = require('../utils/uuid.utils');

class itemController {
  static async get(req, res) {
    const callId = generateUuid();

    console.log('Call %s %s id: %s', req.method, req.url, callId);

    const { id } = req.params;

    try {
      let result;
      if (id) {
        result = await itemService.get(id);
      } else {
        result = await itemService.getAll(req.query);
      }
      return res.status(200).send(result);
    } catch (error) {
      console.log('Call id: %s error:%s', callId, JSON.stringify(error));
      const status = error.status;

      if (status === undefined) return res.status(500).send();

      return res.status(status).send(error);
    }
  }

  static async create(req, res) {
    const callId = generateUuid();

    console.log('Call %s %s id: %s', req.method, req.url, callId);
    console.log('body: ', req.body);

    const { invoiceId, description, price, units, discount } = req.body;

    if (
      (!invoiceId || (invoiceId && typeof invoiceId !== 'number')) ||
      (!price || (price && typeof price !== 'number')) ||
      (!units || (units && typeof units !== 'number')) ||
      (discount && typeof discount !== 'number') ||
      (description && typeof description !== 'string')
    ) {
      console.log('Call id: %s error:%s', callId, 'Required parameter is missing or wrong type');
      return res.status(400).send();
    }

    try {
      console.log('Call id: %s response: success', callId);
      await itemService.create(invoiceId, description, price, units, discount);

      return res.status(201).send();
    } catch (error) {
      console.log('Call id: %s error:%s', callId, JSON.stringify(error));
      const status = error.status;

      if (status === undefined) return res.status(500).send();

      return res.status(status).send(error);
    }
  }

  static async update(req, res) {
    const callId = generateUuid();

    console.log('Call %s %s id: %s', req.method, req.url, callId);
    console.log('body: ', req.body);

    const { description, price, units, discount } = req.body;
    const { id } = req.params;

    if (
      (price && typeof price !== 'number') ||
      (units && typeof units !== 'number') ||
      (discount && typeof discount !== 'number') ||
      (description && typeof description !== 'string')
    ) {
      console.log('Call id: %s error:%s', callId, 'Required parameter is missing or wrong type');
      return res.status(400).send();
    }

    try {
      console.log('Call id: %s response: success', callId);
      await itemService.update(id, description, price, units, discount);

      return res.status(200).send();
    } catch (error) {
      console.log('Call id: %s error:%s', callId, JSON.stringify(error));
      const status = error.status;

      if (status === undefined) return res.status(500).send();

      return res.status(status).send(error);
    }
  }

  static async delete(req, res) {
    const callId = generateUuid();

    console.log('Call %s %s id: %s', req.method, req.url, callId);

    const { id } = req.params;

    try {
      console.log('Call id: %s response: success', callId);
      await itemService.delete(id);

      return res.status(204).send();
    } catch (error) {
      console.log('Call id: %s error:%s', callId, JSON.stringify(error));
      const status = error.status;

      if (status === undefined) return res.status(500).send();

      return res.status(status).send(error);
    }
  }
}

module.exports = itemController;
