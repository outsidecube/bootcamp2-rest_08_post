const invoiceService = require('../services/invoice.service');
const itemService = require('../services/item.service');
const { generateUuid } = require('../utils/uuid.utils');

class invoiceController {
  static async get(req, res) {
    const callId = generateUuid();

    console.log('Call %s %s id: %s', req.method, req.url, callId);

    const { id } = req.params;

    try {
      let result;
      if (id) {
        result = await invoiceService.get(id);
      } else {
        result = await invoiceService.getAll(req.query);
      }
      return res.status(200).send(result);
    } catch (error) {
      console.log('Call id: %s error:%s', callId, JSON.stringify(error));
      const status = error.status;

      if (status === undefined) return res.status(500).send();

      return res.status(status).send(error);
    }
  }
  
  static async create(req, res) {
    const callId = generateUuid();

    console.log('Call %s %s id: %s', req.method, req.url, callId);
    console.log('body: ', req.body);

    const { date, cuit, comment } = req.body;

    if (
      (!date) ||
      (!cuit || cuit && typeof cuit !== 'string') ||
      (comment && typeof comment !== 'string')
    ) {
      console.log(
        'Call id: %s error:%s',
        callId,
        'Required parameter is missing or wrong type'
      );
      return res.status(400).send();
    }

    try {
      console.log('Call id: %s response: success', callId);
      await invoiceService.create(date, cuit, comment);

      return res.status(201).send();
    } catch (error) {
      console.log('Call id: %s error:%s', callId, JSON.stringify(error));
      const status = error.status;

      if (status === undefined) return res.status(500).send();

      return res.status(status).send(error);
    }
  }

  static async update(req, res) {
    const callId = generateUuid();

    console.log('Call %s %s id: %s', req.method, req.url, callId);
    console.log('body: ', req.body);

    const { date, cuit, comment } = req.body;
    const { id } = req.params;

    if (
      (date && typeof date !== 'string') ||
      (cuit && typeof cuit !== 'string') ||
      (comment && typeof comment !== 'string')
    ) {
      console.log(
        'Call id: %s error:%s',
        callId,
        'Required parameter is missing or wrong type'
      );
      return res.status(400).send();
    }

    try {
      console.log('Call id: %s response: success', callId);
      await invoiceService.update(id, date, cuit, comment);

      return res.status(200).send();
    } catch (error) {
      console.log('Call id: %s error:%s', callId, JSON.stringify(error));
      const status = error.status;

      if (status === undefined) return res.status(500).send();

      return res.status(status).send(error);
    }
  }

  static async delete(req, res) {
    const callId = generateUuid();

    console.log('Call %s %s id: %s', req.method, req.url, callId);

    const { id } = req.params;

    try {
      console.log('Call id: %s response: success', callId);
      await itemService.deleteAllInvoiceItems(id);

      await invoiceService.delete(id);
      return res.status(204).send();
    } catch (error) {
      console.log('Call id: %s error:%s', callId, JSON.stringify(error));
      const status = error.status;

      if (status === undefined) return res.status(500).send();

      return res.status(status).send(error);
    }
  }
}

module.exports = invoiceController;
