const invoiceController = require('./invoice.controller');
const invoiceService = require('../services/invoice.service.js');
const itemService = require('../services/item.service.js');

jest.mock('../services/invoice.service.js');
jest.mock('../services/item.service.js');

let sendMock;
let statusMock;
let res;

beforeEach(() => {
  sendMock = jest.fn();
  statusMock = jest.fn();
  res = { status: statusMock, send: sendMock };
  statusMock.mockImplementation(() => res);
});

describe('Invoice Controller', () => {
  describe('create method', () => {
    it('should fail with 400 if date is missing', async () => {
      const req = {
        body: {}
      };

      await invoiceController.create(req, res);
      expect(statusMock).toBeCalledWith(400);
    });

    it('should fail with 400 if cuit is missing', async () => {
      const req = {
        body: {
          date: '2020-03-03'
        }
      };

      await invoiceController.create(req, res);
      expect(statusMock).toBeCalledWith(400);
    });

    it('should pass with 201 and create invoice', async () => {
      const req = {
        body: {
          date: '2020-03-03',
          cuit: '20111111110',
          comment: 'Test'
        }
      };
      const mock = {};
      invoiceService.create.mockImplementationOnce(() => mock);

      await invoiceController.create(req, res);
      expect(statusMock).toBeCalledWith(201);
    });

    it('should return with 500', async () => {
      const req = {
        body: {
          date: '2020-03-03',
          cuit: '20111111110',
          comment: 'Test'
        }
      };
      const mock = {};
      invoiceService.create.mockImplementationOnce(() => {
        throw new Error('Error');
      });

      await invoiceController.create(req, res);
      expect(statusMock).toBeCalledWith(500);
    });
  });

  describe('/put update invoice', () => {
    it('should pass with 200 and update invoice', async () => {
      const req = {
        params: {
          id: 1
        },
        body: {
          comment: 'Test (1)'
        }
      };
      const mock = {};
      invoiceService.update.mockImplementationOnce(() => mock);

      await invoiceController.update(req, res);
      expect(statusMock).toBeCalledWith(200);
    });

    it('should return 400 bad request', async () => {
      const req = { params: { id: 1 }, body: { comment: 1 } };

      await invoiceController.update(req, res);
      expect(statusMock).toBeCalledWith(400);
    });

    it('should return 404 invoce_not_found', async () => {
      const req = { params: { id: 1 }, body: {} };

      invoiceService.update.mockImplementationOnce(() => {
        throw {
          status: 404,
          error: 'invoce_not_found',
          msg: 'Factura no encontrada'
        };
      });

      await invoiceController.update(req, res);
      expect(statusMock).toBeCalledWith(404);
    });

    it('should return with 500', async () => {
      const req = {
        params: {
          id: 1
        },
        body: {
          comment: 'Test (1)'
        }
      };
      const mock = {};
      invoiceService.update.mockImplementationOnce(() => {
        throw new Error('Error');
      });

      await invoiceController.update(req, res);
      expect(statusMock).toBeCalledWith(500);
    });
  });

  describe('delete an invoice', () => {
    it('should succeed with 200 and delete invoice', async () => {
      const req = { params: { id: 1 } };

      const mock = {};

      itemService.deleteAllInvoiceItems.mockImplementationOnce(() => mock);
      invoiceService.delete.mockImplementationOnce(() => mock);

      await invoiceController.delete(req, res);
      expect(statusMock).toBeCalledWith(204);
    });

    it('should return 404 invoce_not_found', async () => {
      const req = { params: { id: 1 } };

      const mock = {};

      itemService.deleteAllInvoiceItems.mockImplementationOnce(() => mock);
      invoiceService.delete.mockImplementationOnce(() => {
        throw {
          status: 404,
          error: 'invoce_not_found',
          msg: 'Factura no encontrada'
        };
      });

      await invoiceController.delete(req, res);
      expect(statusMock).toBeCalledWith(404);
    });

    it('should return 500', async () => {
      const req = { params: { id: 1 } };

      const mock = {};

      itemService.deleteAllInvoiceItems.mockImplementationOnce(() => mock);
      invoiceService.delete.mockImplementationOnce(() => {
        throw new Error('Error');
      });

      await invoiceController.delete(req, res);
      expect(statusMock).toBeCalledWith(500);
    });
  });

  describe('get single invoice', () => {
    it('should pass with 200 ', async () => {
      const req = {
        params: { id: '1' }
      };
      const mock = [
        {
          id: 1,
          date: '2020-03-03T00:00:00.000Z',
          cuit: '20111111110',
          comment: 'Test Comment',
          createdAt: '2020-04-10T21:01:05.000Z'
        }
      ];

      const result = [
        {
          id: 1,
          date: '2020-03-03T00:00:00.000Z',
          cuit: '20111111110',
          comment: 'Test Comment',
          createdAt: '2020-04-10T21:01:05.000Z'
        }
      ];

      invoiceService.get.mockImplementationOnce(() => mock);

      await invoiceController.get(req, res);
      expect(statusMock).toBeCalledWith(200);
      expect(sendMock).toBeCalledWith(expect.objectContaining(result));
    });

    it('should return 404 - invoce_not_found', async () => {
      const req = {
        params: { id: 10 }
      };

      invoiceService.get.mockImplementationOnce(() => {
        throw {
          status: 404,
          error: 'invoce_not_found',
          msg: 'Factura no encontrada'
        }
      });

      await invoiceController.get(req, res);
      expect(statusMock).toBeCalledWith(404);
    });
  });

  describe('get all invoices', () => {
    it('should pass with 200 ', async () => {
      const req = {
        params: {}
      };
      const mock = [
        {
          id: 1,
          date: '2020-03-03T00:00:00.000Z',
          cuit: '20111111110',
          comment: 'Test Comment',
          createdAt: '2020-04-10T21:01:05.000Z'
        },
        {
          id: 2,
          date: '2020-03-03T00:00:00.000Z',
          cuit: '20111111110',
          comment: 'Test Comment',
          createdAt: '2020-04-10T21:01:05.000Z'
        }
      ];

      const result = [
        {
          id: 1,
          date: '2020-03-03T00:00:00.000Z',
          cuit: '20111111110',
          comment: 'Test Comment',
          createdAt: '2020-04-10T21:01:05.000Z'
        },
        {
          id: 2,
          date: '2020-03-03T00:00:00.000Z',
          cuit: '20111111110',
          comment: 'Test Comment',
          createdAt: '2020-04-10T21:01:05.000Z'
        }
      ];

      invoiceService.getAll.mockImplementationOnce(() => mock);

      await invoiceController.get(req, res);
      expect(statusMock).toBeCalledWith(200);
      expect(sendMock).toBeCalledWith(expect.objectContaining(result));
    });

    it('should return 500 ', async () => {
      const req = {
        params: {}
      };

      invoiceService.getAll.mockImplementationOnce(() => {
        throw new Error('Error');
      });

      await invoiceController.get(req, res);
      expect(statusMock).toBeCalledWith(500);
    });
  });
});
