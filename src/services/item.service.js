const invoiceDao = require('../dao/invoice.dao');
const itemDao = require('../dao/item.dao');

class itemService {
  static async create(invoiceId, description, price, units, discount) {
    const exists = await invoiceDao.exists(invoiceId, 'id');
    if (exists[0].exists === 0)
      throw {
        status: 404,
        error: 'invoce_not_found',
        msg: 'Factura no encontrada'
      };

    const item = [invoiceId, description, price, units, discount];
    return itemDao.create(item);
  }

  static async update(id, description, price, units, discount) {
    const exists = await itemDao.exists(id, 'id');
    if (exists[0].exists === 0)
      throw {
        status: 404,
        error: 'item_not_found',
        msg: 'Item no encontrado'
      };

    return itemDao.update(id, description, price, units, discount);
  }

  static async delete(id) {
    const exists = await itemDao.exists(id, 'id');
    if (exists[0].exists === 0)
      throw {
        status: 404,
        error: 'item_not_found',
        msg: 'Item no encontrado'
      };

    return itemDao.delete(id);
  }

  static async get(id) {
    const exists = await itemDao.exists(id, 'id');
    if (exists[0].exists === 0)
      throw {
        status: 404,
        error: 'item_not_found',
        msg: 'Item no encontrado'
      };

    return itemDao.get(id);
  }

  static getAll(filters) {
    return itemDao.getAll(filters);
  }

  static deleteAllInvoiceItems(invoiceId) {
    return itemDao.deleteAllInvoiceItems(invoiceId);
  }
}

module.exports = itemService;
