const invoiceDao = require('../dao/invoice.dao');

class invoiceService {
  static create(date, cuit, comment) {
    const invoice = [date, cuit, comment];
    return invoiceDao.create(invoice);
  }

  static async update(id, date, cuit, comment) {
    const exists = await invoiceDao.exists(id, 'id');
    if (exists[0].exists === 0)
      throw {
        status: 404,
        error: 'invoce_not_found',
        msg: 'Factura no encontrada'
      };

    return invoiceDao.update(id, date, cuit, comment);
  }

  static async delete(id) {
    const exists = await invoiceDao.exists(id, 'id');
    if (exists[0].exists === 0)
      throw {
        status: 404,
        error: 'invoce_not_found',
        msg: 'Factura no encontrada'
      };

    return invoiceDao.delete(id);
  }

  static async get(id) {
    const exists = await invoiceDao.exists(id, 'id');
    if (exists[0].exists === 0)
      throw {
        status: 404,
        error: 'invoce_not_found',
        msg: 'Factura no encontrada'
      };

    return invoiceDao.get(id);
  }

  static async getAll(filters) {
    return invoiceDao.getAll(filters);
  }
}

module.exports = invoiceService;
