const invoiceService = require('./invoice.service');
const invoiceDao = require('../dao/invoice.dao.js');

jest.mock('../dao/invoice.dao.js');

describe('Invoice Service', () => {
  describe('Create method', () => {
    it('should create an invoice', async () => {
      const mock = {};
      invoiceDao.create.mockImplementationOnce(() => mock);

      const result = await invoiceService.create('2020-03-03', '20111111110', 'Test');
      expect(result).toEqual(mock);
    });
  });

  describe('Update method', () => {
    it('should fail with 404 invoce_not_found', async () => {
      const mock = [{exists: 0}];
      const resultError = {
        status: 404,
        error: 'invoce_not_found',
        msg: 'Factura no encontrada'
      };
      invoiceDao.exists.mockImplementationOnce(() => mock);

      try {
        await invoiceService.update(1, '2020-01-01', '20111111110', 'Test');
      } catch (error) {
        expect(error).toEqual(resultError);
      }
    });

    it('should update an invoice', async () => {
      const mock = {};
      invoiceDao.exists.mockImplementationOnce(() => [{exits: 1}]);
      invoiceDao.update.mockImplementationOnce(() => mock);

      const result = await invoiceService.update(1, '2020-03-03', '20111111110', 'Test');
      expect(result).toEqual(mock);
    });
  });

  describe('Delete method', () => {
    it('should fail with 404 invoce_not_found', async () => {
      const mock = [{exists: 0}];
      const resultError = {
        status: 404,
        error: 'invoce_not_found',
        msg: 'Factura no encontrada'
      };
      invoiceDao.exists.mockImplementationOnce(() => mock);

      try {
        await invoiceService.delete(1);
      } catch (error) {
        expect(error).toEqual(resultError);
      }
    });

    it('should delete an invoice', async () => {
      const mock = {};
      invoiceDao.exists.mockImplementationOnce(() => [{exits: 1}]);
      invoiceDao.delete.mockImplementationOnce(() => mock);

      const result = await invoiceService.delete(1);
      expect(result).toEqual(mock);
    });
  });

  describe('Get method', () => {
    it('should fail with 404 invoce_not_found', async () => {
      const mock = [{exists: 0}];
      const resultError = {
        status: 404,
        error: 'invoce_not_found',
        msg: 'Factura no encontrada'
      };
      invoiceDao.exists.mockImplementationOnce(() => mock);

      try {
        await invoiceService.get(1);
      } catch (error) {
        expect(error).toEqual(resultError);
      }
    });

    it('should get an invoice', async () => {
      const mock = {};
      invoiceDao.exists.mockImplementationOnce(() => [{exits: 1}]);
      invoiceDao.get.mockImplementationOnce(() => mock);

      const result = await invoiceService.get(1);
      expect(result).toEqual(mock);
    });

    it('should get all invoices', async () => {
      const mock = {};
      invoiceDao.getAll.mockImplementationOnce(() => mock);

      const result = await invoiceService.getAll();
      expect(result).toEqual(mock);
    });
  });
});
