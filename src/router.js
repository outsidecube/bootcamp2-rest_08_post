const express = require('express');
const userController = require('./controllers/user.controller');
const invoiceController = require('./controllers/invoice.controller');
const itemController = require('./controllers/item.controller');
const router = express.Router();
const version = 'v1';

/* User */

router.post(`/${version}/users`, userController.signUp);
router.put(`/${version}/users/:id`, userController.update);
router.delete(`/${version}/users/:id`, userController.delete);
router.get(`/${version}/users/:id`, userController.get);

/*
    1) Implementar los metodos necesarios para que la API Rest tenga Facturas e Items
        getInvoices(all || invoiceId) [filtros (date, cuit, comment)]
        getInvoice(invoiceId)
        postInvoice()
        putInvoice(invoiceId)
        deleteInvoice(invoiceId)

        getItems(all || itemId) [filtros (invoiceId, description, price, units, discount)]
        getItem(itemId)
        putItem(itemId)
        postItem()
        deleteItem(itemId)
*/

router.get(`/${version}/invoices/:id?`, invoiceController.get);
router.post(`/${version}/invoices`, invoiceController.create);
router.put(`/${version}/invoices/:id`, invoiceController.update);
router.delete(`/${version}/invoices/:id`, invoiceController.delete);

router.get(`/${version}/items/:id?`, itemController.get);
router.post(`/${version}/items`, itemController.create);
router.put(`/${version}/items/:id`, itemController.update);
router.delete(`/${version}/items/:id`, itemController.delete);

/*
    2) Implementar Express ACL para validar permisos para el uso de la API Rest
    3) Implementar el Swagger de nuestra API Rest (usuario, facturas, items)
*/

module.exports = {router, version};
